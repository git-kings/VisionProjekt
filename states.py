import statemachine
import socket
import cv2

# State in a state machine that starts a server for communication.
class StartServer(statemachine.State):
    def Execute(self):
        # Define the host (IP address) and port for the server.
        HOST = '192.168.0.5' # Computer ip/Raspberry Pi 
        PORT = 9090 # Avoid using reserved ports
        self.stateMachine.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Accepting connections # Socket for hosting
        self.stateMachine.serverSocket.bind((HOST, PORT)) # Binds host and port
        self.stateMachine.serverSocket.listen() # Listen for client
        print(f"Server listening on {HOST}:{PORT}")
        self.stateMachine.clientSocket, address =  self.stateMachine.serverSocket.accept() # Communication accept
        print(f"connected to {address}") 
        self.stateMachine.ChangeState(ConnectionToUR()) # Change state til ConnectionToUR

# State in a state machine that handles communication with a Universal Robot (UR) client.
class ConnectionToUR(statemachine.State): 
    def Execute(self):
        msg = self.stateMachine.clientSocket.recv(1024).decode('utf-8') # Recive message (status) from UR client
        print("Message from robot:", msg)

        # Check if message indicates that robot is ready
        if msg =='ready': # If robot is ready change to ProcessImage state
            self.stateMachine.ChangeState(ProcessImage())
        else: # If the message does not indicate that robots is ready stop the state machine
            self.stateMachine.ChangeState(StopServer)
           

class ProcessImage(statemachine.State): # State in a state machine that where image is processed pipeline.
    def Enter(self):                    # The state captures a frame from the camera, processes it using a defined pipeline in 'visionStateMachine.py'
                                        # And defines relevant information such as robot coordinates, object rotation and type
        frame = self.stateMachine.cam.getFrame() # Capture frame from camera
        processed = self.stateMachine.pipeline.run(frame) # Run frame through the pipeline
        cv2.waitKey(1)

        # If no shapes are found, transition to the StopServer state
        if len(processed) < 1:
            self.stateMachine.ChangeState(StopServer())
            return
        
        shape = processed[0] # Examine first iteration of the list obtained from the pipeline.
        
        # Find robot cordinates by converting pixels cordinates to robot cordinates
        self.stateMachine.robotCordX = -0.0004080522543209110373*shape.center[0]+(-0.0000101695265874370352)*shape.center[1]+0.37145373113641770357
        self.stateMachine.robotCordY = -0.000010346914078257361823*shape.center[0]+(0.00041525615167406212185)*shape.center[1]+0.19074427995159338441

        # Find obejct rotation
        rect = cv2.minAreaRect(shape.contour)
        self.stateMachine.rotation = rect[2]
        if (rect[1][0] < rect[1][1]):
            self.stateMachine.rotation = self.stateMachine.rotation - 90
        self.stateMachine.rotation = self.stateMachine.rotation*0.0175533

        # Determine type of object
        if shape.area <= 3440:
            self.stateMachine.objType  = 0 # Fræser
            self.stateMachine.fræsere += 1 # Adding 1 to numberes of fræsere used for data log
            
        else:
            self.stateMachine.objType  = 1 # Adapter
            self.stateMachine.adaptere += 1 # Adding 1 to numberes of adptere used for data log
        
        self.stateMachine.ChangeState(GrabObject())
        


class GrabObject(statemachine.State): # State in the state machine responsible for letting the robot grab and move the detected object.
    def Enter(self):
        msg = f"({self.stateMachine.robotCordX},{self.stateMachine.robotCordY},{self.stateMachine.robotCordZ},{self.stateMachine.rotation},{self.stateMachine.objType})"  
        print(msg) # Message containing information used in the robot program
        self.stateMachine.clientSocket.sendall(msg.encode('ascii'))
        
        self.stateMachine.ChangeState(ConnectionToUR())
    

class LogData(statemachine.State): # State where data is saved in a log
    def Enter(self):
        # Create data string
        dataToWrite = ('Number of adapters sorted: ')
        dataToWrite += str(self.stateMachine.adaptere) # Number of adapteres sorted
        dataToWrite += ('\nNumber of fræsere sorted: ') 
        dataToWrite += (str(self.stateMachine.fræsere)) # Number of fræsere sorted
        print(dataToWrite)

        # Save data str to file
        dataFile = open("dataLog.txt", "w")
        dataFile.write(dataToWrite)
        dataFile.close()

        # Shut down state machine
        self.stateMachine.running = False
    

class StopServer(statemachine.State): #  This class represents a state in the state machine where the server stops
    def Enter(self):
        print("No Objects Found\nClosing Server")
        self.stateMachine.serverSocket.close() # Close the server
        self.stateMachine.ChangeState(LogData()) 
from statemachine import State, StateMachine 
import socket
from IMPP import *
import cv2
import OAKWrapper
import time


# This class is used to make additions to the original state machine "statemachine.py"
class VisionStateMachine(StateMachine):
    def __init__(self, state: State):
        super().__init__(state)
        self.serverSocket = socket.socket()
        self.clientSocket = socket.socket()
        self.rotation = []
        self.robotCordX = 0
        self.robotCordY = 0
        self.robotCordZ = 0.18972
        self.objType = 0
        self.adaptere = 0
        self.fræsere = 0
        self.cam = OAKWrapper.OAKCamColor(previewHeight=500, previewWidth=500)
        time.sleep(4)
        self.cam.setAutoFocusMode()
        self.pipeline = PostProcessingPipeline([ # Pipeline for image processing
                        AverageBlur(3, showOutput=False), # Blur image for better processing
                        ConvertRGB2HSV(showOutput=True, outputWindowName='ConvertBGR2HSV'), # Convert image to HSV
                        HSVThreshold(lowerBound=np.array([0,0,0]), upperBound=np.array([255,255,80]), outputMask=True, showOutput=False, outputWindowName='HSVThreshold'), #Threshold med hsv
                        Dilate(iterations=3, showOutput=False, outputWindowName='Dilate'), # Dialate + Erode --> Closing
                        Erode(iterations=3, showOutput=False, outputWindowName='Erode'), 
                        DetectContours(cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE, printResult=False, draw=False, outputWindowName='DetectContours'), # finder contours and returns a list 
                        ThresholdContours(minArea=0, maxArea=200000, printDebug=True), # ThresHold filters the list with detected contours # Returns in a list
                        DetectShapes(closed=True, epsilon=0.001, printResult=True) #DetectShapes finds shapes in the filtered list
])
